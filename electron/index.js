document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 11111;

// var server_addr = "127.0.0.1";   // the IP address of your Raspberry 
var server_addr = "192.168.0.115"; 

var nodeConsole = require('console');
var con = new nodeConsole.Console(process.stdout, process.stderr);
var dist = 0;
var startTime;
var endTime;

function client(){
    
    const net = require('net');
    var input = document.getElementById("message").value;

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        con.log('connected to server!');
        con.log(input);
        // send the message
        client.write("S" + input);
    });
    
    // get the data from the server
    client.on('data', (data) => {
        con.log("receive data")
        con.log(data.toString());
        d = JSON.parse(data.toString());
        con.log(d["temp"]);

        if ('bluetooth' in d) {
            document.getElementById("bluetooth").innerHTML = d["bluetooth"];
        }
        if ('temp' in d) {
            document.getElementById("temperature").innerHTML = d["temp"];
        }
        if ('speed' in d) {
            document.getElementById("speed").innerHTML = d["speed"];
        }
        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });
}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;

    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        send_data("87");
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        send_data("83");
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        send_data("65");
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        send_data("68");
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}


// update data for every second
function update_data(){
    setInterval(function(){
        client();
    }, 1000);
}

function send(cmd){
    const net = require('net');
    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        con.log('connected to server!');
        con.log(cmd);
        client.write("C" + cmd);
    });
    client.on('data', (data) => {
        con.log("receive control data")
        con.log(data.toString());
        d = JSON.parse(data.toString());
        document.getElementById("direction").innerHTML = d["direction"];
        document.getElementById("speed").innerHTML = d["speed"];
        document.getElementById("temperature").innerHTML = d["temp"];
        startTime = d["startTime"];
        document.getElementById("distance").innerHTML = dist.toFixed(2);
        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });

}
    
function stop(){
    const net = require('net');

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        con.log('stop');
        // send the message
        client.write("!STOP");
    });

    client.on('data', (data) => {
        con.log(data.toString());
        d = JSON.parse(data.toString());
        document.getElementById("direction").innerHTML = d["direction"];
        document.getElementById("speed").innerHTML = d["speed"];
        con.log(typeof d["direction"]);

        if(prev_status == 'forward' || prev_status == 'backward'){
            endTime = d["endTime"];
            dist += (endTime - startTime) * 20;     
            document.getElementById("distance").innerHTML = dist.toFixed(2);
        }
        console.log(data.toString());
        client.end();
        client.destroy();
    });
    client.on('end', () => {
        console.log('disconnected from server');
    });
}

function forward(){
    con.log('move forward!');
    var cmd = "f";
    send(cmd);
    prev_status = "forward";
}

function backward(){
    con.log('move backward!');
    var cmd = "b";
    send(cmd);
    prev_status = "backward";
}

function turnLeft(){
    con.log('turn Left!');
    var cmd = "l";
    send(cmd);
    prev_status = "turnLeft";
}

function turnRight(){
    con.log('turn Right!');
    var cmd = "r";
    send(cmd);
    prev_status = "turnRight";
}
