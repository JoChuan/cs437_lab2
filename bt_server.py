import bluetooth
import picar_4wd as fc
import time
import random
import os
import subprocess
import json

hostMACAddress = "DC:A6:32:80:7D:87" # The address of Raspberry PI Bluetooth adapter on the server. The server might have multiple Bluetooth adapters.
port = 0
backlog = 1
size = 1024
speed = 30

s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.bind((hostMACAddress, port))
s.listen(backlog)
print("listening on port ", port)
try:
    client, clientInfo = s.accept()
    while 1:   
        print("server recv from: ", clientInfo)
        data = client.recv(size)
        if data == "f":
            fc.forward(speed)
            print(data)
            client.send(data)
        if data == "b":
            fc.backward(speed)
            print(data)
            client.send(data)
        if data == "l":
            fc.turn_right(speed)
            print(data)
            client.send(data)
        if data == "r":
            fc.turn_left(speed)
            print(data)
            client.send(data) 
        if data == "s":
            fc.stop()
            print(data)
            client.send(data)
        if data == "t":
            temp = subprocess.run(['vcgencmd', 'measure_temp'], stdout = subprocess.PIPE)
            temp = str(temp.stdout)
            temp = temp[7:].strip('\\n\"')
            print(temp)
            client.send(temp)

except: 
    print("Closing socket")
    client.close()
    s.close()

